addGroup = (svgCode, colorCode) => {
  const gTagOpen = `<g fill="${colorCode}">`
  const gTagClose = `</g>`
  const regex1 = />/g
  const regex2 = /</g
  const index1 = [...svgCode.matchAll(regex1)][0]["index"] + 1
  const arr = [...svgCode.matchAll(regex2)]
  const index2 = arr[arr.length - 1]["index"]
  const finalSVG = svgCode.slice(0,index1) + gTagOpen + svgCode.slice(index1, index2) + gTagClose + svgCode.slice(index2, svgCode.length)

  return finalSVG
}

active = (el) => {
  arr = document.getElementsByClassName('icon')
  for(var i=0;i<arr.length;i++){
    arr[i].classList.remove('iconActive')
  }
  el.classList.add("iconActive")
}

downloadIcon = () =>{
  colorCode = document.getElementById('color').value
  svg  = document.getElementsByClassName('iconActive')[0]
}