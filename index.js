const fs = require('fs')

//PATH TO SVG FILE
const svgFile = './animated-svg/loading.svg'

//ENTER YOUR COLOR CODE HERE
const colorCode='#d1d120'

const svgCode = fs.readFileSync(svgFile, 'utf8')

modifySVG = (svgCode, colorCode) => {
  const gTagOpen = `<g fill="${colorCode}">`
  const gTagClose = `</g>`
  const regex1 = />/g
  const regex2 = /</g
  const index1 = [...svgCode.matchAll(regex1)][0]["index"] + 1
  const arr = [...svgCode.matchAll(regex2)]
  const index2 = arr[arr.length - 1]["index"]
  const finalSVG = svgCode.slice(0,index1) + gTagOpen + svgCode.slice(index1, index2) + gTagClose + svgCode.slice(index2, svgCode.length)

  return finalSVG
}

const modifiedSvgCode = modifySVG(svgCode, colorCode)
newPath = './temp/' + svgFile.split('/')[2]

fs.writeFile(newPath, modifiedSvgCode, (err) => { 
      
    // In case of a error throw err. 
    if (err) throw err; 
})


